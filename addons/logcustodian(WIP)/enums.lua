local enums = {};

enums.reaction = {
    [0x00] = 'None',
    [0x01] = 'Miss',
    [0x03] = 'Parry',
    [0x04] = 'Block',
    [0x08] = 'Hit',
    [0x09] = 'Miss', -- "Evade" (Miss + Hit)
    [0x14] = 'Guard',
};


enums.chatmode = {
    [1]  = 20, -- Melee attack
    [2]  = 20, -- Ranged attack finish
    [3]  = 20, -- WS finish
    [4]  = 56, -- Casting finish
    [5]  = 101, -- Item finish
    [6]  = 101, -- JA finish
    [7]  = 52, -- WS start
    [8]  = 52, -- Casting start
    [9]  = 90, -- Item start
    [10] = 52, -- JA start
    [11] = 20, -- Mob ability finish
    [12] = 20, -- Ranged attack start
    [13] = 20, -- Pet mob ability finish
    [14] = 20, -- Dance
    [15] = 1, -- Quarry
    [16] = 1, -- Sprint
};

enums.blockedmodes = {
    [20]  = true; -- Self hits
    [21]  = true; -- Self misses enemy
    [22]  = true; -- Enemy drains hp from self
    [24]  = true; -- Party member recovers hp/mp
    [25]  = true; -- Party member hits enemy
    [26]  = true; -- Party member aborbs hit
    [27]  = true; -- Enemy drains hp from party member
    [28]  = true; -- Enemy hits self
    [29]  = true; -- Enemy misses self
    [30]  = true; -- Self drains hp from enemy
    [31]  = false; -- Self cured by party member or other
    [32]  = true; -- Enemy hits party member
    [33]  = true; -- Enemy misses party member
    [34]  = true; -- Party member drains mp from enemy
    [35]  = false; -- Party member recovers hp
    [36]  = false; -- Self defeats enemy
    [37]  = false; -- Party member defeats enemy
    [40]  = true; -- Other hits enemy
    [41]  = true; -- Enemy misses other
    [42]  = true; -- Enemy drains hp from other (p member's pet?)
    [44]  = false; -- Other defeats enemy
    [50]  = true; -- Self starts casting
    [51]  = true; -- Party member starts casting
    [52]  = true; -- Self/other starts casting
    [56]  = true; -- Self gains effect
    [59]  = false; -- No effect on self
    [61]  = false; -- Party member receives debuff
    [64]  = true; -- Party member gains effect
    [65]  = true; -- Enemy gains effect
    [67]  = true; -- No effect (on enemy?)
    [69]  = true; -- Spell has no effect (enemy?)
    [85]  = true; -- Party member uses item
    [90]  = true; -- Self uses item
    [100] = true; -- Enemy readies tp move on self
    [101] = true; -- Self uses JA
    [102] = true; -- Enemy uses tp move (on self, status?)
    [104] = true; -- Enemy misses tp move (on self?)
    [105] = true; -- Enemy readies tp move on party member
    [106] = true; -- Party member uses JA
    [109] = true; -- Enemy misses tp move of party member
    [110] = true; -- Self readies WS
    [111] = true; -- Self uses JA on party member
    [112] = false; -- Other receives debuff
    [114] = true; -- Self JA misses enemy
    [121] = false; -- Loot drop, target out of range (examine, trade)
    [122] = false; -- Out of range/unable to see (melee), intimidated, interrupt (!), lacks tools
    [123] = false; -- You must wait longer to perform that action, effect wearing off
    [127] = false; -- Loot obtained (self?)
    [129] = false; -- Skillup
    [131] = false; -- XP obtained (self?)
    [141] = false; -- Cannot attack that target
    [163] = true; -- Alliance member hits enemy
    [168] = true; -- Alliance member starts casting
    [171] = true; -- Alliance member uses item
    [175] = false; -- Alliance member uses JA (dancer?)
    [177] = true; -- Enemy readies JA on alliance member
    [186] = true; -- Alliance member misses enemy
    [191] = false; -- Status effect wears off
    [391] = false; -- Party invite
}

enums.messageid = {
    [1]   = 'Hits',
    [2]   = 'Takes damage',
    [15]  = 'Misses',
    [75]  = 'No effect',
    [85]  = 'Resists',
    [93]  = 'Vanishes',
    [230] = 'Gains effect',
    [236] = 'is', -- silenced, bound, poisoned etc.
    [237] = 'Receives the effect of'

}
return enums;