

require('common');
local enums = require('enums');


-- Helper function for retrieving the zone index from serverID
local helpers = {};

helpers.getentityindexbyid = function(entityid)
	if (entityid < 10000000) then -- for PCs
		for i=1024, 2303 do
			if (AshitaCore:GetMemoryManager():GetEntity():GetServerId(i) == entityid) then
				return i;
			end
		end
	else -- NPCs (including pets)
		return bit.band(entityid, 0x7FF);
	end
end

helpers.idlength = function(actiontype)
	return switch(actiontype, {
		[3]  = function () return 10; end,
		[4]  = function () return 10; end,
		[5]  = function () return 16; end,
		[6]  = function () return 12; end,
		[11] = function () return 12; end,
		[14] = function () return 12; end,
		[switch.default] = function () return 10; end,
	});
end

helpers.handleparams = function(paramtable, type)
    return switch(type, {
		-- Melee attacks
        [1] = function ()
			local result = 0;
			local hits = 0;
			for _, v in ipairs(paramtable.baseparams) do
				result = result + v;
			end
			for k, v in ipairs(paramtable.reactions) do
				if (v == 0x08) then
					hits = hits + 1;
				end
			end
			if (hits > 1) then
				return '(' + hits + ' hits) \x1E\x44' + result;
			elseif (paramtable.numberofactions == 1) then
				local reaction = enums.reaction[paramtable.reactions[1]];
				if (reaction == 'Hit') then
					return '(' + reaction + ') \x1E\x44' + result;
				else
					if (paramtable.messageid[1] == 0) then
						return '(Absorbed by 1 shadow)'
					else
						return '(' + reaction + ')';
					end
				end
			else
				return '(0 hits)';
			end
		end,
		-- Ranged attack
		[2]  = function ()
			local reaction = enums.reaction[paramtable.reactions[1]];
			local result = paramtable.baseparams[1];
			if (reaction ~= 'Hit') then
				result = "";
			end
			return '(' + reaction + ') ' + result;
		end,
		[3]  = function () return '(' + AshitaCore:GetResourceManager():GetAbilityById(paramtable.actionid).Name[1] + ') ' + paramtable.baseparams[1]; end, -- WS finish
		-- Casting finish
		[4]  = function ()
			if (paramtable.messageid[1] == 2) then -- Nuke/damage
				return '(' + AshitaCore:GetResourceManager():GetSpellById(paramtable.actionid).Name[1] + ') \x1E\x44' + paramtable.baseparams[1];
			elseif (paramtable.messageid[1] == 75) then -- No effect
				return AshitaCore:GetResourceManager():GetSpellById(paramtable.actionid).Name[1] + ' (No effect)';
			elseif (paramtable.messageid[1] == 85) then -- Resist
				return AshitaCore:GetResourceManager():GetSpellById(paramtable.actionid).Name[1] + ' (Resist)';
			elseif (paramtable.messageid[1] == 93) then -- Vanish
				return AshitaCore:GetResourceManager():GetSpellById(paramtable.actionid).Name[1] + ' (Vanishes)';
			elseif (paramtable.messageid[1] == 230) then -- Buff
				return '(' + AshitaCore:GetResourceManager():GetString('buffs.names', paramtable.baseparams[1]) + ')';
			elseif (paramtable.messageid[1] == 236) then -- Debuffed
				return AshitaCore:GetResourceManager():GetSpellById(paramtable.actionid).Name[1] + ' (Receives effect)';
			elseif (paramtable.messageid[1] == 237) then -- Receives effect (debuff)
				return AshitaCore:GetResourceManager():GetSpellById(paramtable.actionid).Name[1] + ' (Receives effect)';
			else
				return paramtable.baseparams[1];
			end
		end,
		[5]  = function () return '(' + AshitaCore:GetResourceManager():GetItemById(paramtable.actionid).Name[1] + ')'; end, -- Item finish
    	[6]  = function () return '(' + AshitaCore:GetResourceManager():GetAbilityById(512 + paramtable.actionid).Name[1] + ')'; end, -- JA finish
    	 -- WS start
		[7]  = function ()
			if (AshitaCore:GetMemoryManager():GetEntity():GetType(paramtable.actorindex) == 0) then
				return '(' + AshitaCore:GetResourceManager():GetAbilityById(paramtable.baseparams[1]).Name[1] + ')';
			elseif (paramtable.baseparams[1] ~= 0) then -- Mob ability start
				return '(' + AshitaCore:GetResourceManager():GetString('monsters.abilities', paramtable.baseparams[1] - 256) + ')';
			else
				return '(interrupted)';
			end
		end,
		-- Spell start
		[8]  = function ()
			if (paramtable.baseparams[1] ~= 0) then
				return '(' + AshitaCore:GetResourceManager():GetSpellById(paramtable.baseparams[1]).Name[1] + ')';
			else
				return '(interrupted)';
			end
		end,
		[9]  = function () return '(' + AshitaCore:GetResourceManager():GetItemById(paramtable.baseparams[1]).Name[1] + ')'; end, -- Item start
		[10] = function () return AshitaCore:GetResourceManager():GetAbilityById(paramtable.baseparams[1]).Name[1]; end, -- JA start
    	[11] = function () return '(' + AshitaCore:GetResourceManager():GetString('monsters.abilities', paramtable.actionid - 256) + ') ' + paramtable.baseparams[1]; end, -- Mob ability finish
    	[12] = function () return paramtable.baseparams[1]; end, -- Ranged attack start
    	[13] = function () return paramtable.baseparams[1]; end, -- Pet mob ability finish
    	[14] = function () return '(' + AshitaCore:GetResourceManager():GetAbilityById(512 + paramtable.actionid).Name[1] + ')'; end, -- Dance
    	[15] = function () return paramtable.baseparams[1]; end, -- Quarry
    	[16] = function () return paramtable.baseparams[1]; end, -- Sprint
		[switch.default] = function () return paramtable.baseparams[1]; end,

            
        
    });
end

return helpers;
