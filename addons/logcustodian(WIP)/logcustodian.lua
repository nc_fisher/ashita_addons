addon.name      = 'logcustodian';
addon.author    = 'Nutrients';
addon.version   = '1.0';
addon.desc      = 'Allows customization of in-game log messages.';
addon.link      = '';

require('common');
local chat = require('chat');
local enums = require('enums');
local help = require('helpers');



ashita.events.register('packet_in', 'packet_in_cb', function (e)
    -- Action packet (auto-attacks, RA, JA, WS, spells, enemy TP moves)
	if (e.id == 0x0028) then
		local params = {};
		local actorid = struct.unpack('I', e.data, 0x05 + 1); -- Get ID of actor from packet
		params.actorindex = help.getentityindexbyid(actorid); -- Convert to zone index for use with ashitacore functions
		
		
		local status, actorname = pcall(
			function ()
				return AshitaCore:GetMemoryManager():GetEntity():GetName(params.actorindex);
			end
		)
		-- When zoning and not fully loaded
		if (not status) then
			actorname = '(Unknown)';
		end

		local actiontype = ashita.bits.unpack_be(e.data_raw, 0x0A, 2, 4);
		local targetnames = {};
		
		local addeffectcounter = 0;
		local spikeeffectcounter = 0;
		local numberoftargets = ashita.bits.unpack_be(e.data_raw, 0x09, 0, 10);
		local totalnumberofactions = 0;

		params.actionid 	= ashita.bits.unpack_be(e.data_raw, 0x0A, 6, help.idlength(actiontype));
		--print(AshitaCore:GetMemoryManager():GetEntity():GetType(params.actorindex))
		params.numberofactions = 0;
		params.baseparams 	= {};
		params.reactions	= {};
		params.speceffect   = {};
		params.messageid    = {};
		params.aetype 		= {};
		params.aeparams 	= {};
		params.spiketype 	= {};
		params.spikeparams 	= {};
		for i=1, numberoftargets do -- Loop for multiple targets (i.e. AoEs)
			local id = ashita.bits.unpack_be(e.data_raw, 0x12, 6 + ((i - 1) * 36) + (totalnumberofactions * 87) + (37 * addeffectcounter) + (34 * spikeeffectcounter), 32);
			targetnames[i] = AshitaCore:GetMemoryManager():GetEntity():GetName(help.getentityindexbyid(id));

			params.numberofactions = ashita.bits.unpack_be(e.data_raw, 0x16, 6 + ((i - 1) * 36) + (totalnumberofactions * 87) + (37 * addeffectcounter) + (34 * spikeeffectcounter), 4); -- i.e. number of hits in an attack round
			
			for j=1, params.numberofactions do
				
				params.reactions[totalnumberofactions + 1] = ashita.bits.unpack_be(e.data_raw, 0x17, 2 + ((i - 1) * 36) + (37 * addeffectcounter) + (34 * spikeeffectcounter) + (totalnumberofactions * 87), 5);
				params.speceffect[totalnumberofactions + 1] = ashita.bits.unpack_be(e.data_raw, 0x19, 1 + ((i - 1) * 36) + (37 * addeffectcounter) + (34 * spikeeffectcounter) + (totalnumberofactions * 87), 7);
				params.baseparams[totalnumberofactions + 1] = ashita.bits.unpack_be(e.data_raw, 0x1A, 5 + ((i - 1) * 36) + (37 * addeffectcounter) + (34 * spikeeffectcounter) + (totalnumberofactions * 87), 17);
				params.messageid[totalnumberofactions + 1] = ashita.bits.unpack_be(e.data_raw, 0x1C, 6 + ((i - 1) * 36) + (37 * addeffectcounter) + (34 * spikeeffectcounter) + (totalnumberofactions * 87), 10);

				if (ashita.bits.unpack_be(e.data_raw, 0x21, 7 + ((i - 1) * 36) + (37 * addeffectcounter) + (34 * spikeeffectcounter) + (totalnumberofactions * 87), 1) == 1) then -- Check for AE
					params.aetype[totalnumberofactions + 1] = ashita.bits.unpack_be(e.data_raw, 0x22, ((i - 1) * 36) + (37 * addeffectcounter) + (34 * spikeeffectcounter) + (totalnumberofactions * 87), 10); -- Type of AE
					params.aeparams[totalnumberofactions + 1] = ashita.bits.unpack_be(e.data_raw, 0x23, 2 + ((i - 1) * 36) + (37 * addeffectcounter) + (34 * spikeeffectcounter) + (totalnumberofactions * 87), 17);
					addeffectcounter = addeffectcounter + 1;
				end
				if (ashita.bits.unpack_be(e.data_raw, 0x22, ((i - 1) * 36) + (37 * addeffectcounter) + (34 * spikeeffectcounter) + (totalnumberofactions * 87), 1) == 1) then -- Check for spikes
					params.spiketype[totalnumberofactions + 1] = ashita.bits.unpack_be(e.data_raw, 0x22, 1 + ((i - 1) * 36) + (37 * addeffectcounter) + (34 * spikeeffectcounter) + (totalnumberofactions * 87), 10); -- Type of spikes
					params.spikeparams[totalnumberofactions + 1] = ashita.bits.unpack_be(e.data_raw, 0x23, 3 + ((i - 1) * 36) + (37 * addeffectcounter) + (34 * spikeeffectcounter) + (totalnumberofactions * 87), 17);
					spikeeffectcounter = spikeeffectcounter + 1;
				end
				totalnumberofactions = totalnumberofactions + 1;
			end
		end
		
		--Print message
		--print(params.messageid[1])
		AshitaCore:GetChatManager():AddChatMessage(enums.chatmode[actiontype], false, string.format('%s %s \129\168 %s', actorname, help.handleparams(params, actiontype), table.concat(targetnames,', ')));
	
	-- Action message packet (loot, out-of-range message, effect wears off etc.)
	elseif (e.id == 0x0029) then
	end
end);

-- Intercept and hide normal battle text
ashita.events.register('text_in', 'text_in_cb', function (e)
	if (((e.mode ~= 214 and e.mode > 15) or (e.mode < 0)) and not e.injected) then
		e.message_modified = e.message_modified + '\x1E\x06 chat mode: ' + tostring(e.mode);
	end
	if ((enums.blockedmodes[e.mode]) and not e.injected) then
		e.blocked = true;
	end
end);